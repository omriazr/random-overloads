/* eslint-disable */
//---------------------------------------------
//     Function overloads challenge
//---------------------------------------------
/**
 * Implement a function called *random* which returns a random number
 * we can optionaly pass a *max* range to pick from
 * we can optionaly pass a *min* number to start the range
 * we can optionally pass a *float* boolean to indicate if we want a float (true) or an integer (false)
 *   the *default is false* for the float boolean
 * if we call random with *no parameters*, it should *return either 0 or 1*
 * -----------------------
 * implement the function and call it in all variations
 * log the results for the different use cases
 */

import { markAsUntransferable } from "worker_threads";

// function random() {

// }

const num1 = random(); // 0 ~ 1 | integer
const num2 = random(true); // 0 ~ 1 | float
const num3 = random(false, 6); // 0 ~ 6 | integer,
const num4 = random(false, 2, 6); // 2 ~ 6 | integer
const num5 = random(true, 6); // 0 ~ 6 | float
const num6 = random(true, 2, 6); // 2 ~ 6 | float

function random():number;
function random(returnFloat:boolean):number;
function random(returnFloat:boolean, num1:number):number;
function random(returnFloat:boolean, num1:number, num2:number):number;

function random(arg1?:boolean, arg2?:number,arg3?:number):number{
    if(arg1 === true){
        if(arg2 === undefined && arg3 === undefined){
            return Math.random();
        } else if(arg3 === undefined){
            return (Math.random() * Number(arg2));
        } else {
            return ((Math.random() * (Number(arg3) - Number(arg2)) + Number(arg2))); 
        }

    } else if(arg1 === false){
        if(arg2 && arg3 === undefined){
            return Math.round((Math.random() * Number(arg2)));
        } else if(arg2 && arg3){
            return Math.round(((Math.random() * (Number(arg3) - Number(arg2)) + Number(arg2)))); 
        } 
    }
    return Math.round(Math.random());
}


console.log({ num1, num2, num3, num4, num5, num6 });

//---------------------------------------------------------------

